/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */
#include <pic18.h>
#include "picebs2_canlib/can.h"
#include "picebs2_canlib/lift.h"
#endif

/******************************************************************************/
/* Interrupt Routines                                                         */
/******************************************************************************/
/* High-priority service */

void interrupt high_isr(void)
{
    if((CAN_INTF == 1)&&(CAN_INTE == 1)) // interrupt flag & active
    {
        CAN_INTF = 0;               // clear interrupt
        Can_Isr();                  // interrupt treatment
        if(CAN_INTPIN == 0)         // check pin is high again
        {
          CAN_INTF = 1;             // no -> re-create interrupt
        }
    }
    
    if(TMR3IF == 1 && TMR3IE ==1){       
        TMR3IF = 0;             //clear flag
        TMR3ON = 0;             //Disable timer0
      
        setDoor(0);    //Close the door
        canGoToNext=1;         
    }
    
    if(TMR1IF == 1 && TMR1IE == 1){
        TMR1IF = 0;
        TMR1ON = 0;
        setLightLift(0);
    }       
}
