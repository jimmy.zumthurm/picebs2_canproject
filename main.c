/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>        /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>       /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>   /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */

#endif
#define _XTAL_FREQ 64000000L

#include "picebs2_canlib/can.h"
#include "picebs2_lcdlib/lcd_highlevel.h"
#include "picebs2_canlib/lift.h"
#include "picebs2_canlib/timers.h"

/******************************************************************************/
/* User Global Variable Declaration                                           */
/******************************************************************************/

/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/

uint8_t get_liftID();
void setCanFilter(uint8_t mask, uint8_t id, CANFILTER * filter);


void initTimer3(){
     //Timer de 4sec
    T3CONbits.TMR3CS1 = 1;
    T3CONbits.TMR3CS0 = 0;
    T3CONbits.T3CKPS1 = 0;   //Prescaler = 2
    T3CONbits.T3CKPS0 =1;
    T3CONbits.SOSCEN = 1;   //SOSC enable (32kHz)
    T3CONbits.nT3SYNC = 1;  //Do not synchronize external clock input
    T3CONbits.RD16 = 1;     //16-Bit mode
    T3CONbits.TMR3ON = 0;    //Stop timer
    TMR3=0;
    
    TMR3IF = 0;
    TMR3IE = 1;
    PEIE = 1;
    GIE = 1;

}

void initTimer1(){
    //Timer de 8sec
    T1CONbits.TMR1CS1 = 1;
    T1CONbits.TMR1CS0 = 0;
    T1CONbits.T1CKPS1 =1;   //Prescaler = 4
    T1CONbits.T1CKPS0 =0;
    T1CONbits.SOSCEN = 1;   //SOSC enable (32kHz)
    T1CONbits.nT1SYNC = 1;  //Do not synchronize external clock input
    T1CONbits.RD16 = 1;     //16-Bit mode
    T1CONbits.TMR1ON = 0;    //Stop timer
    TMR1=0;
    
    TMR1IF = 0;
    TMR1IE = 1;
    PEIE = 1;
    GIE = 1;
}

void main(void)
{
    /* Configure the oscillator for the device works @ 64MHz*/
   PLLEN = 1;            // activate PLL x4
   OSCCON = 0b01110000;  // for 64MHz cpu clock (default is 8MHz)
    // Caution -> the PLL needs up to 2 [ms] to start !
    __delay_ms(2); 
    /* Initialize I/O and Peripherals for application */ 

    uint8_t mask = 0x7E0;
    CANFILTER filter;
    
    //init can communication so we can send a request for the lift ID, first
    //mask that takes all
    setCanFilter(mask, 0x7E0, &filter);
    Can_Init(&filter);
    GIE = 1;
    //get the lift ID
    lift_id = get_liftID();
    
    //once we got the lift ID, we need to change our filters and mask to 
    //communicate with our lift only
    mask = 0x01F;
    setCanFilter(mask,lift_id, &filter);
    Can_Init(&filter);   
    
    resetLift();        //after the communication is correctly set
    initLift();
    initTimer3();
    initTimer1();
    
    CANMESSAGE message;
    while(1)
    {
        if(Can_GetMessage(&message) == CAN_OK){
            update(message.identifier, message.dta);
        }    
        driveLift();
    }
}

uint8_t get_liftID(){
    CANMESSAGE request;
    CANMESSAGE answer;
    uint8_t id;
    
    request.identifier= 0x7FF;
    request.extended_identifier = 0;
    request.rtr = 0;
    request.txPrio = 3;
    request.dlc = 0;

    Can_PutMessage(&request);
    
    while(Can_GetMessage(&answer) != CAN_OK){};
    
    id = answer.identifier & 0x1F;
    
    return id;
}

void setCanFilter(uint8_t mask, uint8_t id, CANFILTER * filter){ 
    filter->ext0=0;
    filter->ext1=0;
    filter->filter0 = id;
    filter->filter1 = id; 
    filter->filter2 = id; 
    filter->filter3 = id;
    filter->filter4 = id;
    filter->filter5 = id;
    filter->mask0 = mask;
    filter->mask1 = mask;
}