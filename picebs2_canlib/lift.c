/******************************************************************************/
/* FILENAME     : LIFT.C                                                      */
/*----------------------------------------------------------------------------*/
/* VERSION      : 1                                                           */
/*----------------------------------------------------------------------------*/
/* REVISION     : -                                                           */
/******************************************************************************/
#include "lift.h"
#include "can.h"
#include <stdbool.h>
#include <stdio.h>

void initLift(){
    led_state_floorDown = 0x00;
    led_state_floorUp = 0x00;
    led_state_lift = 0x00;
    request_nbr = 0;
	current_floor = 0;
   for(uint8_t i=0; i<N_STAGE; i++){
        destination[i]=0xFF;    //Value 0->7 are stage so we putt FF
    }
}

void update(uint16_t can_id, uint8_t data[]){
    uint16_t answer = can_id - lift_id;
    switch(answer){
        case BUTTON_LIFT:   
            if(data[1]==0){ //if button is pressed
                if(!(position%INCREMENTS==0 && (floorDataConverter(data[0])==position/INCREMENTS))){
                    setLedLift(data[0], 1);	//turn on led
                }                
                setGoal(data[0]);
            }
            break;  
           
        case BUTTON_FLOOR:
            //data[button UP bit, button DOWN bit, 0=pressed 1=release]
            if(data[0]!=0){  //if button up is pressed
                if(data[2]==0){
                    if(!(position%INCREMENTS==0 && (floorDataConverter(data[0])==position/INCREMENTS))){
                        setLedFloor(data[0], UP, 1);
                    }                    
                    setGoal(data[0]);
                }
            }
            else{           
                if(data[2]==0){ //if button down is pressed
                    if(!(position%INCREMENTS==0 && (floorDataConverter(data[1])==position/INCREMENTS))){
                        setLedFloor(data[1], DOWN, 1);
                    }
                    setGoal(data[1]);
                }
            }
                
            break;
            
        case STATUS_RESPONSE:
            current_speed = data[1];
            
            if(current_speed==0 && door_state== DOOR_CLOSED){
                canOpenDoor=1;    
            }
            
            door_state = data[2];
            if(data[0] != 0)
                current_floor = floorDataConverter(data[0]);            
            break;
            
        case ERROR:
            errorFlag = 1;
            break;
            
        case RAIL_LIFT:           
            if(speed>0){
                position++;  
            }                
            else if(speed<0){
                position--;
            }
            break;
    }
}

void setLedLift(uint8_t led, uint8_t state){
    CANMESSAGE led_lift;    
    if(state == 1)
    {
        led_state_lift |= led;
    }
    else
    {
        led_state_lift &= ~led;
    }
    
    led_lift.identifier = lift_id + LED_LIFT;   //CAN ID
    led_lift.dta[0] = led_state_lift;   //Each bit of the variable is a LED
    led_lift.dlc = 1;                   //dta is one byte long
    led_lift.txPrio = 1;                //priority from 0 to 3
    led_lift.extended_identifier = 0;   //0 = 11 bits, 1 = 29 bits
    led_lift.rtr=0;
    //led_lift.filhit=
    
    Can_PutMessage(&led_lift);
}

void setLedFloor(uint8_t led, uint8_t direction, uint8_t state){
    CANMESSAGE led_floor;
    if(direction==0){
        if(state == 1)
        {
            led_state_floorUp = led_state_floorUp | led;
        }
        else
        {
            led_state_floorUp = led_state_floorUp & ~led;
        }
    }
    else{
        if(state == 1)
        {
            led_state_floorDown = led_state_floorDown | led;
        }
        else
        {
            led_state_floorDown = led_state_floorDown & ~led;
        }
    }
    
    
    led_floor.identifier = lift_id + LED_FLOOR;   //CAN ID
    led_floor.dta[0] = led_state_floorUp;
    led_floor.dta[1] = led_state_floorDown;
    led_floor.dlc = 2;                   //dta is one byte long
    led_floor.txPrio = 1;                //priority from 0 to 3
    led_floor.extended_identifier = 0;   //0 = 11 bits, 1 = 29 bits
    led_floor.rtr=0;
    //led_lift.filhit=
    
    Can_PutMessage(&led_floor);
}

void setDoor(uint8_t state){
    CANMESSAGE door_lift;
    door_lift.identifier = lift_id + DOOR_STATE;   //CAN ID
    door_lift.dta[0] = state;      //1 = we open it, 0 = we close it
    door_lift.dlc = 1;                   //dta is one byte long
    door_lift.txPrio = 1;                //priority from 0 to 3
    door_lift.extended_identifier = 0;   //0 = 11 bits, 1 = 29 bits
    door_lift.rtr=0;
    //door_lift.filhit=
    
    Can_PutMessage(&door_lift);
}

uint8_t floorDataConverter(uint8_t data){
    uint8_t retval=0;
    uint8_t floor = data;
    while(floor!=1){    //Convert value of floor (1->128) to (0->7)
        floor/=2;
        retval++;
    }
    return retval;
}

void setLightLift(uint8_t state){
    CANMESSAGE light_lift;
    light_lift.identifier = lift_id + LIGHT_CAB;    //CAN ID
    light_lift.dta[0] = state;          //message up to 8 bytes
    light_lift.dlc = 1;
    light_lift.txPrio = 1;              //Priority (0 to 3, 3 is max)
    light_lift.extended_identifier = 0; 
    light_lift.rtr=0;

    Can_PutMessage(&light_lift);        //send message 
}

void setGoal(uint8_t floor){
    uint8_t convFloor= floorDataConverter(floor);   
    
	for(uint8_t i=0; i<N_STAGE; i++){		//Search if we already asked this stage
		if(destination[i] == convFloor){//If yes
			return;                     //We get out of this
		}
	}
    
    destination[request_nbr] = convFloor;		//If not, we add the destination in the array
	
	request_nbr++;
    TMR1=0;
    T1CONbits.TMR1ON=0;
    setLightLift(1);
}

void driveLift(){
    int i;
    uint8_t floorLED=1;
    
    //first thing to check : if we got an error we need to reset
    if(errorFlag==1){
        resetLift();
        return;
    }
    
    //useless to optimize anything if we have 1 call only
    if(request_nbr>1){
        destinationOptimisation();
    }   
           
    //destination array is correctly sorted, we can now drive the lift     
    
    if(destination[0] == current_floor){
    //the target position is reached
        
        if (canGoToNext==1){
             //Convert LED number
            for(i=0;i<destination[0];i++){
                floorLED*=2;
            }            
            setLedFloor(floorLED, UP, 0);
            setLedFloor(floorLED, DOWN, 0);
            setLedLift(floorLED, 0 );
        }
        
        canGoToNext=0;        
         
        //stop motor
        speed=0;
        setMotor(); 
        
        if(canOpenDoor==1){
            
            for(i=0;i<N_STAGE;i++){
                if(i<request_nbr)
                    destination[i]=destination[i+1];
                else
                    destination[i]=0xFF;
            }
            request_nbr--; 
            
            if(request_nbr==0){
                T1CONbits.TMR1ON=1;
            }
            
            setDoor(1);
            T3CONbits.TMR3ON=1;
        }         
    }
    else if(destination[0]-current_floor > 0 && destination[0] != 0xFF && door_state == DOOR_CLOSED && canGoToNext==1){
    //the target position is above the lift        
        if(position>=destination[0]*INCREMENTS-MARGIN_INCREM && position<destination[0]*INCREMENTS){
            speed = LOW_SPEED;
            setMotor();
        }
        else {
            speed = HIGH_SPEED;
            setMotor();
        }   
    }
    else if(destination[0]-current_floor < 0 && destination[0] != 0xFF && door_state == DOOR_CLOSED && canGoToNext==1){
    //the target position is below the lift        
        if(position<=destination[0]*INCREMENTS+MARGIN_INCREM && position>destination[0]*INCREMENTS){
            speed = -LOW_SPEED;
            setMotor();
        }
        else {
            speed = -HIGH_SPEED;
            setMotor();
        }           
    }
}

void setMotor(){
    static int8_t lastSpeed=0;
    CANMESSAGE motor_speed;
    
    if(speed!=0){
        canOpenDoor=0;
    }
    if(speed != lastSpeed)
    {
        lastSpeed = speed;
        motor_speed.dlc = 1;
        motor_speed.dta[0] = speed;
        motor_speed.txPrio = 3;
        motor_speed.identifier = lift_id + MOTOR_SPEED;
        motor_speed.extended_identifier = 0;
        motor_speed.rtr=0;

        Can_PutMessage(&motor_speed); 
    }
}

void destinationOptimisation(){
    /*
     Logic is the following : 
     * We don't want to make clients wait too long, but we want to stop 
     * if there is a floor called that is inbetween the goal and the current
     * floor
     */
    int i;
    int temp;
    int temp1;
    int difference=destination[0]-current_floor;
    int smallestDiff = difference;
    int closestIndex=0;
    int closestIndexValue;
    
    //lift needs to move up
    if(difference>0){
        for(i=1;i<request_nbr;i++){
            //the destination we compare with the current closest one, must be above the current floor (>0)
            if(destination[i]-current_floor<smallestDiff && destination[i]-current_floor>0){
                smallestDiff=destination[i]-current_floor;
                closestIndex=i;
            }
        }        
    }
    //lift needs to move down
    else if(difference<0){
        for(i=1;i<request_nbr;i++){
            //the destination we compare with the current closest one, must be below the current floor (<0)
            if(destination[i]-current_floor>smallestDiff && destination[i]-current_floor<0){
                smallestDiff=destination[i]-current_floor;
                closestIndex=i;
            }
        }
    }
    //destination is reached, other function handles this case, we do nothing
    else{
        return;
    }
    
    //if destination[0] is already the closest, we do nothing
    if(closestIndex==0){
        return;
    }
    
    //if we are too close to the elemnent to put in first pos and lift is moving we do nothing as well
    if(current_speed!=0){
        if((difference > 0 && (position >= destination[closestIndex]*INCREMENTS - MARGIN_INCREM && position <= destination[closestIndex]*INCREMENTS))
        ||(difference < 0 && (position <= destination[closestIndex]*INCREMENTS + MARGIN_INCREM && position >= destination[closestIndex] ))){
            return;
        }
    }
         
    //remove the closest element from the array, first memorize it
    closestIndexValue = destination[closestIndex];        
    for(i=closestIndex;i<N_STAGE;i++){
        if(i<request_nbr)
            destination[i]=destination[i+1];
        else
            destination[i]=0xFF;
    }
    //right shift of array
    temp=destination[0];
    for(i=1;i<N_STAGE;i++){
        temp1=destination[i];
        destination[i]=temp;
        temp=temp1;
    }
    destination[0] = closestIndexValue;

    for(i=0;i<N_STAGE;i++){
        if(i>=request_nbr){
            destination[i]=0xFF;
        }
    }
}

void resetLift(){
    CANMESSAGE request;
    CANMESSAGE answer;
    int i;
    uint8_t led=1;
    
    errorFlag=0;
    
    request.identifier = lift_id + 0x0C0;
    request.extended_identifier = 0;
    request.rtr = 0;
    request.dlc = 0;
    request.txPrio = 3;
    
    
    Can_PutMessage(&request);
    
    request.identifier = lift_id + 0x0A0;
    
    Can_PutMessage(&request);
    
    //sutck in loop if the R floor is not reached yet
    while(!(Can_GetMessage(&answer) == CAN_OK && (answer.identifier-lift_id)==0x0A0 && answer.dta[0]==1)){}
  
    //remove all the destinations and turn off all leds
    for(i=0;i<N_STAGE;i++){
        destination[i]=0xFF;
        setLedFloor(led, UP, 0);
        setLedFloor(led, DOWN, 0);
        setLedLift(led, 0 );
        led*=2;        
    }    
    
    //reset global variables
    current_floor=0;
    speed=0;
    position=0;
    canGoToNext=1;
    canOpenDoor=1;
}
