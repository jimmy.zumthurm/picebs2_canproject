/* 
 * File:   lift.h
 * Author: SIERRO
 *
 * Created on 28. mai 2020, 13:45
 */

#ifndef LIFT_H
#define	LIFT_H

#ifdef	__cplusplus
extern "C" {
#endif
#ifdef	__cplusplus
}
#endif
#endif	/* LIFT_H */

/////////////////////////////////////////
//////  My code starts here /////////////
/////////////////////////////////////////
#include <pic18.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

/*
 * Define all the commands from controller to the lift
 */
#define MOTOR_SPEED     0X020
#define LED_LIFT        0X040
#define LED_FLOOR       0X060
#define DOOR_STATE      0X080
#define STATUS_REQUEST  0X0A0   //non used
#define RESET_LIFT      0X0C0
#define LIGHT_CAB       0X0E0

/*
 * Define all the answer from lift to the controller
 */
#define BUTTON_LIFT     0x100
#define BUTTON_FLOOR    0X120
#define STATUS_RESPONSE 0X0A0
#define ERROR           0X140
#define RAIL_LIFT       0X300

/*
 * Define we need
 */
#define DOOR_CLOSED     0x00
#define DOOR_OPENING    0x01
#define DOOR_CLOSING    0x02
#define DOOR_OPENED     0x03
#define N_STAGE			0x08
#define UP              0x00
#define DOWN            0x01
#define INCREMENTS      11
#define MARGIN_INCREM   3
#define HIGH_SPEED      60
#define LOW_SPEED       10
/*********************************/
/***********Global value**********/
/*********************************/
uint8_t lift_id;
uint8_t door_state;         //open, opening, close, closing
uint8_t led_state_lift;     //State of all lift's leds
uint8_t led_state_floorUp;  //State of all up leds
uint8_t led_state_floorDown;//State pf all down leds
uint8_t current_floor;	
uint8_t destination[N_STAGE];    //How many request do we have in the destination array
uint8_t request_nbr;
int8_t speed; //speed of the lift
uint8_t current_speed;
uint8_t position; //from 0 to 77
uint8_t canGoToNext;
uint8_t canOpenDoor;
uint8_t errorFlag;

/**
 * Initialize the lift
 */
void initLift();

/**
 * This function tests wich answers from the lift is received
 * @param can_id    Lift ID + command
 * @param data[]    Message Data 
 */
void update(uint16_t can_id, uint8_t data[]);

/**
 * This function change the LED's state of the button in the lift
 * @param led       wich LED
 * @param state     1 = on, 0 = off 
 */
void setLedLift(uint8_t led, uint8_t state);

/**
 * This function changes the LED's state on the floor
 * @param led       wich LED has been pressed
 * @param direction LED up or down
 * @param state     1=0n, 0=off
 */
void setLedFloor(uint8_t led, uint8_t direction, uint8_t state);

/**
 * Close or open the door
 * @param state     0 = we close it, 1 = we open it  
 */
void setDoor(uint8_t state);

/**
 * set all destination asked in an array
 * @param floor     wich floor we must go
 */
void setGoal(uint8_t floor);

/**
 * Optimize the destination array to serve the clients well
 */
void destinationOptimisation();

/**
 * set the motor speed
 */
void setMotor();

/**
 * main function that is periodically called in the main loop
 */
void driveLift();

/**
 * Reset the lift, stuck in a loop until the lift has reached the R floor
 */
void resetLift();

/**
 * 
 * @param data The Floor (
 * @return 
 */
uint8_t floorDataConverter(uint8_t data);

/**
 * Set state of the light in the lift
 * @param state 1=on, 0=off
 */
void setLightLift(uint8_t state);