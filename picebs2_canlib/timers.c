/******************************************************************************/
/* FILENAME     : LIFT.C                                                      */
/*----------------------------------------------------------------------------*/
/* VERSION      : 1                                                           */
/*----------------------------------------------------------------------------*/
/* REVISION     : -                                                           */
/******************************************************************************/
#include "timers.h"
#include "can.h"
#include <stdbool.h>
#include <pic18.h>

void initTimer(){
    //Timer0 of 4sec
    T0CONbits.TMR0ON = 0;   //Disable timer0
    T0CONbits.T08BIT = 0;   //16bits timer
    T0CONbits.T0CS = 0;     //Internal clock Fosc/4
    T0CONbits.T0SE = 0;     //Inrement low-to-high transition
    T0CONbits.PSA = 0;      //Prescaler is assigned
    T0CONbits.T0PS = 0b110; //Prescaler = 128
}
